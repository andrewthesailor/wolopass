package com.wololo.wolopass.dao.impl;

import com.wololo.wolopass.dao.PasswordDao;
import com.wololo.wolopass.dto.Password;
import com.wololo.wolopass.dto.User;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import java.util.List;


public class PasswordDaoImpl implements PasswordDao {

    private static PasswordDaoImpl instance;
    private EntityManager entityManager;

    public static PasswordDaoImpl getInstance() {
        if (instance == null) {
            instance = new PasswordDaoImpl();
            instance.entityManager = Persistence.createEntityManagerFactory("persistence").createEntityManager();
        }
        return instance;
    }


            @Override
    public List<Password> getUserPasswords(User user) {
        TypedQuery<Password> query = entityManager.createQuery("SELECT p from Password p WHERE p.owner = :user",Password.class);
        query.setParameter("user",user);
        return query.getResultList();
    }

    @Override
    public void storePassword(Password password) {
        entityManager.getTransaction().begin();
        entityManager.persist(password);
        entityManager.getTransaction().commit();
    }

    @Override
    public void removeElement(Password password) {
        entityManager.getTransaction().begin();
        entityManager.remove(password);
        entityManager.getTransaction().commit();

    }


}
