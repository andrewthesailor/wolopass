package com.wololo.wolopass.dao.impl;

import com.wololo.wolopass.dao.UserDao;
import com.wololo.wolopass.dto.User;
import com.wololo.wolopass.helpers.SecurityHelper;


import javax.persistence.*;


public class UserDaoImpl implements UserDao {

    private static UserDaoImpl instance;
    private EntityManager entityManager;

    public static UserDaoImpl getInstance() {
        if (instance == null) {
            instance = new UserDaoImpl();
            instance.entityManager = Persistence.createEntityManagerFactory("persistence").createEntityManager();
        }
        return instance;

    }


    @Override
    public User getUserByName(String name) {

        TypedQuery<User> query = entityManager.createQuery("SELECT usr from User usr WHERE usr.name = :user_name", User.class);
        query.setParameter("user_name", name);
        try {
            User user = query.getSingleResult();
            return user;
        }catch(NoResultException ex){
            return null;
        }
    }

    @Override
    public void storeUser(User user) throws NonUniqueResultException {

        if(getUserByName(user.getName())!=null){
            throw new NonUniqueResultException();
        }

        entityManager.getTransaction().begin();
        entityManager.persist(user);
        entityManager.getTransaction().commit();
    }

    @Override
    public User authenticateUser(String name, String password) {
        User user = getUserByName(name);
        if(user!=null && user.getPassword().equals(SecurityHelper.hashString(password))){
            return user;
        }
        return null;
    }
}
