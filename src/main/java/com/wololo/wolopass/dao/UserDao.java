package com.wololo.wolopass.dao;

import com.wololo.wolopass.dto.User;

import javax.persistence.NonUniqueResultException;

public interface UserDao {
    public User getUserByName(String name);

    public void storeUser(User user) throws NonUniqueResultException;

    public User authenticateUser(String name, String password);

}
