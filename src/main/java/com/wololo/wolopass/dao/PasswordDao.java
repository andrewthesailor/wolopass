package com.wololo.wolopass.dao;

import com.wololo.wolopass.dto.Password;
import com.wololo.wolopass.dto.User;

import java.util.List;

public interface PasswordDao {
    public List<Password> getUserPasswords(User user);
    public void storePassword(Password password);
    public void removeElement(Password password);
}
