package com.wololo.wolopass.dto;



import javax.persistence.*;

@Entity
@Table(name = "PASSWORDS")
public class Password {

@Id
@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "s_pas_id")
    @Column(name = "pas_id")
    private Long Id;

@Column(name = "pas_name")
    private String name;

@Column(name = "pas_pass")
    private String password;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "pas_usr_id")
    private User owner;

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
