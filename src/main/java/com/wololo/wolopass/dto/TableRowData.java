package com.wololo.wolopass.dto;

import com.wololo.wolopass.helpers.SecurityHelper;
import javafx.beans.property.SimpleStringProperty;
import javafx.scene.control.Button;


public class TableRowData {
    private final SimpleStringProperty passname;
    private final SimpleStringProperty decodedPass;
    private Button deleteButton;
    private Password password;

    public TableRowData(Password pass,String secret){
        passname=new SimpleStringProperty(pass.getName());
        decodedPass = new SimpleStringProperty(SecurityHelper.decrypt(pass.getPassword(),secret));
        password= pass;
        deleteButton=new Button("Delete");
    }

     public String getName(){
        return passname.get();
     }

     public String getPass(){
        return decodedPass.get();
     }

    public Button getDeleteButton() {
        return deleteButton;
    }
}
