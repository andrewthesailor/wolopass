package com.wololo.wolopass.helpers;

import com.wololo.wolopass.dto.User;

public class UserHelper {
    private static UserHelper instance;
    private User user;
    private String secret;

    public static UserHelper getInstance(){
        if(instance==null){
            instance=new UserHelper();
        }
        return instance;
    }

    public void setData(User user, String password){
        this.user=user;
        this.secret=password;

    }

    public String getSecret() {
        return secret;
    }

    public User getUser() {
        return user;
    }
}
