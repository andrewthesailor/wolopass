package com.wololo.wolopass.start;


import com.wololo.wolopass.dao.impl.PasswordDaoImpl;
import com.wololo.wolopass.dao.impl.UserDaoImpl;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application{

    public static void main(String[] args)
    {

        launch(args);

    }


    @Override
    public void start(Stage primaryStage) throws Exception {
        UserDaoImpl.getInstance();
        PasswordDaoImpl.getInstance();
        Parent root = FXMLLoader.load(getClass().getResource("/fxml/screens/loginScreen.fxml"));
        Scene scene = new Scene(root, 600, 600);

        primaryStage.setTitle("wolopass");
        primaryStage.setScene(scene);
        primaryStage.show();

    }
}
