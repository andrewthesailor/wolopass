package com.wololo.wolopass.controller;

import com.wololo.wolopass.dao.impl.UserDaoImpl;
import com.wololo.wolopass.dto.User;
import com.wololo.wolopass.helpers.SecurityHelper;
import com.wololo.wolopass.helpers.UserHelper;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextArea;
import javafx.scene.text.Text;
import javafx.stage.Stage;


public class LoginController {
    @FXML private TextArea loginField;

    @FXML private PasswordField passField;

    @FXML private Text errorField;

    @FXML protected void handleLoginButton(ActionEvent event){
        User user=UserDaoImpl.getInstance().authenticateUser(loginField.getText(),passField.getText());
        if(user!=null){
            login(user,passField.getText());
        }
        else{
            errorField.setText("Wrong data");
        }
    }

    @FXML protected void handleRegisterButton(ActionEvent event){
        if(UserDaoImpl.getInstance().getUserByName(loginField.getText())!=null){
            errorField.setText("Username busy");
        }
        else{
            User user=new User();
            user.setName(loginField.getText());
            user.setPassword(SecurityHelper.hashString(passField.getText()));
            UserDaoImpl.getInstance().storeUser(user);
            login(user,passField.getText());
        }
    }

    private void login(User user, String password){


        Scene scene = loginField.getScene();
        try {
            Parent root = FXMLLoader.load(getClass().getResource("/fxml/screens/passwordScreen.fxml"));
            Stage primaryStage= (Stage)(scene.getWindow());
            UserHelper.getInstance().setData(user, password);
            primaryStage.setScene(new Scene(root,1000,600));
        }catch(Exception ex){
            System.out.println(ex.getMessage());
        }

    }

}
