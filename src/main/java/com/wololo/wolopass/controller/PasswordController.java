package com.wololo.wolopass.controller;

import com.wololo.wolopass.dao.impl.PasswordDaoImpl;
import com.wololo.wolopass.dto.Password;
import com.wololo.wolopass.dto.TableRowData;
import com.wololo.wolopass.helpers.SecurityHelper;
import com.wololo.wolopass.helpers.UserHelper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;

import javafx.scene.control.TableColumn;
import javafx.scene.control.cell.PropertyValueFactory;

import java.net.URL;
import java.util.ResourceBundle;

public class PasswordController {//implements Initializable {

    @FXML
    private TextField name;

    @FXML private  TextField passwordField;

    @FXML private TableView tableView;

    ObservableList<TableRowData> data;

    /*
    @Override
    public void initialize(URL location, ResourceBundle resources) {
*/

        public PasswordController(){

        TableColumn nameCol = new TableColumn("Password Name");
        TableColumn passCol = new TableColumn("Password");
        TableColumn actionCol = new TableColumn("Action");

        tableView.getColumns().addAll(nameCol,passCol,actionCol);

        data = FXCollections.observableArrayList();

        for(Password pass:UserHelper.getInstance().getUser().getPasswords()){
            data.add(new TableRowData(pass,UserHelper.getInstance().getSecret()));
        }

        nameCol.setCellValueFactory(new PropertyValueFactory<TableRowData,String>("passname"));
        passCol.setCellValueFactory(new PropertyValueFactory<TableRowData,String>("decodedPass"));
        actionCol.setCellValueFactory(new PropertyValueFactory<TableRowData,String>("deleteButton"));
        tableView.setItems(data);

    }



    @FXML protected void addPassword(){
        Password password = new Password();
        password.setName(name.getText());
        password.setPassword(SecurityHelper.encrypt(passwordField.getText(),UserHelper.getInstance().getSecret()));
        PasswordDaoImpl.getInstance().storePassword(password);
        data.add(new TableRowData(password,UserHelper.getInstance().getSecret()));
    }



}
